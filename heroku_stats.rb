#!/usr/bin/env ruby
require_relative 'src/PlabLogParser.rb'
require_relative 'src/PlabHerokuStatistics.rb'

#Since command line interface is so primitive I decide not use OptionParser
default_input = 'sample.log'
$source_log = ARGV.length == 1 ? ARGV[0] : default_input
unless File.exist? $source_log
  STDERR.puts "File \"#{$source_log}\" doesn't exist."
  STDERR.puts 'Usage: heroku_stats.rb [input_filename]'
  STDERR.puts "\tIn case no filename provided '#{default_input}' is tested"
  exit 1
end

#in future should be put as command line param with default value but for test i keep it hardcoded
api_call_filter = ['GET/count_pending_messages','GET/get_messages','GET/get_friends_progress','GET/get_friends_score','POST/','GET/']
$statistics = PlabHerokuStatistics.new
$parser = PlabLogParser.new api_call_filter

File.open($source_log).each do |line|
    $parser.parse_line line,$.
    if $parser.valid?
      $statistics.function_call $parser.apicall
    end
end

##Print results
puts 'Call statistics (sorted by most frequent calls)'
$statistics.get_by_most_frequent.each do |callname|
    puts "\tCall #{callname} #{$statistics.api_calls[callname].print_stats}"
end

puts "\nGlobally most used source/dyno #{$statistics.get_top_dyno_name}"
