require_relative 'ApiCall.rb'

class PlabLogParser
  attr_reader :apicall
  def initialize filter
    @valid = false
    @call_filter = filter
    @apicall = ApiCall.new
  end
  def valid?
    return @valid
  end
  def parse_line line, line_cnt=0
    @valid = false
    # Normally i would ignore spaces enclosed in "" but sample logfile doesn't have such line (verified)
    # However check for it and warn just in case in future logfile format changes it is easy to find where is problem
    elems = line.split(' ')
    unless elems.count == 12
      STDERR.puts "WARNING: log line #{line_cnt} contains unexpected amount of space separated elements. Ignore!"
      return nil
    end

    method = elems[3][7..-1]
    if elems[4][0..15] == 'path=/api/users/' and elems[4][16..-1] =~ /^\d+\/?(.*)/
      @apicall.name = method + '/' + $1
      if @call_filter.include? @apicall.name
        @apicall.dyno = elems[7][5..-1]
        time_cnct = elems[8][8..-3].to_i
        time_serv = elems[9][8..-3].to_i
        @apicall.time = time_cnct + time_serv
        @valid = true
        return @apicall
      end
    end
    return nil
  end
end
