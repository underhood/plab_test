class ApiCallStats
  attr_accessor :count, :response_times, :dyno_stats

  def initialize
    @count = 0
    @response_times = Array.new
    @dyno_stats = Hash.new(0)
  end

  def get_top_dyno_name
    @dyno_stats.keys.sort_by { |key| @dyno_stats[key] }.reverse![0]
  end

  def get_average_time
    @response_times.inject{ |sum, el| sum + el }.to_f / @response_times.size
  end

  def get_median_time
    @response_times.sort! #we can modify
    len = @response_times.length
    median = (@response_times[(len - 1) / 2] + @response_times[len / 2]) / 2.0
  end

  def print_stats
    return "\n\t\tcalled #{@count} times\n\t\tTopDyno: #{get_top_dyno_name}\n\t\tAvg. Response Time: #{sprintf('%.2f', get_average_time)}ms\n\t\tMedian: #{sprintf('%.2f', get_median_time)}ms"
  end
end
