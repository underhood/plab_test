require_relative 'ApiCall.rb'
require_relative 'ApiCallStats.rb'

class PlabHerokuStatistics
  attr_reader :api_calls
  def initialize
    @api_calls = Hash.new
    @global_dyno_stats = Hash.new(0) #independent of api call name
  end
  def function_call call
    @api_calls[call.name] = ApiCallStats.new unless @api_calls.has_key? call.name

    @api_calls[call.name].count += 1
    @api_calls[call.name].response_times.push call.time
    @global_dyno_stats[call.dyno] += 1
    @api_calls[call.name].dyno_stats[call.dyno] += 1
  end
  def get_by_most_frequent
    @api_calls.keys.sort_by { |key| @api_calls[key].count }.reverse!
  end
  def get_top_dyno_name
    @global_dyno_stats.keys.sort_by { |key| @global_dyno_stats[key] }.reverse![0]
  end
end
