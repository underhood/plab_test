require_relative '../src/PlabLogParser.rb'

require 'test/unit'

class TestPlabLogParser < Test::Unit::TestCase
  def setup
    @parser = PlabLogParser.new ['GET/count_pending_messages','GET/get_messages','GET/get_friends_progress','GET/get_friends_score','POST/','GET/']
  end
  def test_valid_line
    @parser.parse_line '2014-01-09T06:16:53.742892+00:00 heroku[router]: at=info method=GET path=/api/users/100002266342173/count_pending_messages host=services.pocketplaylab.com fwd="94.66.255.106" dyno=web.8 connect=9ms service=9ms status=304 bytes=0'
    assert(@parser.valid?, 'Parser should be in valid state in this test')
    assert(@parser.apicall.name == 'GET/count_pending_messages', 'Wrong call name')
    assert(@parser.apicall.dyno == 'web.8', 'Wrong dyno name')
    assert(@parser.apicall.time == 18, 'Wrong time (connect+service)')
  end
  def test_filtering
    @parser.parse_line '2014-01-09T06:16:53.748849+00:00 heroku[router]: at=info method=POST path=/api/online/platforms/facebook_canvas/users/100002266342173/add_ticket host=services.pocketplaylab.com fwd="94.66.255.106" dyno=web.12 connect=12ms service=21ms status=200 bytes=78'
    assert(!@parser.valid?, 'Parser should return invalid because parsed line should be skipped')
  end
  def test_malformedline
    @parser.parse_line '2014-01-09T06:16:53.748849+00:00 heroku[router]: at=info method=POST path=/api/online/platforms/facebook_canvas/users/100002266342173/add_ticket host=services.pocketplaylab.com fwd="94.66.255.106" dyno=web.12 connect=12ms service=21ms'
    assert(!@parser.valid?, 'Parser should be reporting invalid state')
  end
  def test_misvalidation
    @parser.parse_line '2014-01-09T06:16:53.742892+00:00 heroku[router]: at=info method=GET path=/api/users/100002266342173/count_pending_messages host=services.pocketplaylab.com fwd="94.66.255.106" dyno=web.8 connect=9ms service=9ms status=304 bytes=0'
    assert(@parser.valid?, 'Parser should be in valid state in this test')
    @parser.parse_line '2014-01-09T06:16:53.748849+00:00 heroku[router]: at=info method=POST path=/api/online/platforms/facebook_canvas/users/100002266342173/add_ticket host=services.pocketplaylab.com fwd="94.66.255.106" dyno=web.12 connect=12ms service=21ms'
    assert(!@parser.valid?, 'Parser should be reporting invalid state')
  end
end
