require_relative '../src/PlabHerokuStatistics.rb'

require 'test/unit'

#this test will test ApiCallStats class as well
class TestPlabHerokuStatistics < Test::Unit::TestCase
  def test_1
    @statistics = PlabHerokuStatistics.new
    call = ApiCall.new

    call.name = 'GET/call_a'
    call.dyno = 'web.11'
    call.time = 1
    @statistics.function_call call
    @statistics.function_call call

    call.name = 'GET/call_a'
    call.dyno = 'web.8'
    call.time = 2
    @statistics.function_call call

    call.name = 'GET/call_a'
    call.dyno = 'web.2'
    call.time = 3
    @statistics.function_call call

    call.name = 'POST/call_b'
    call.dyno = 'web.1'
    call.time = 3
    @statistics.function_call call

    assert(@statistics.get_top_dyno_name == 'web.11', 'Wrong most called dyno (globaly not api call specific)')
    assert_equal(2, @statistics.get_by_most_frequent.size, 'Wrong array size')
    assert(@statistics.get_by_most_frequent[0] == 'GET/call_a', 'Wrong most called function')
    assert(@statistics.get_by_most_frequent[1] == 'POST/call_b', 'Wrong 2nd most called function')

    assert_equal(4, @statistics.api_calls['GET/call_a'].count, 'The call count of function GET/call_a doesn\'t match reality.')
    assert_equal(1, @statistics.api_calls['POST/call_b'].count, 'The call count of function GET/call_a doesn\'t match reality.')

    assert_equal(1.75,@statistics.api_calls['GET/call_a'].get_average_time, 'Average time calculated wrong!')
    assert_equal(1.5,@statistics.api_calls['GET/call_a'].get_median_time, 'Median calculated wrong')
    assert_equal('web.11',@statistics.api_calls['GET/call_a'].get_top_dyno_name, 'Top dyno for GET/call_a is wrong')

    assert_equal(3, @statistics.api_calls['POST/call_b'].get_average_time, 'The avg time of function POST/call_b doesn\'t match reality.')
    assert_equal(3, @statistics.api_calls['POST/call_b'].get_median_time, 'The median time of function POST/call_b doesn\'t match reality.')
    assert_equal('web.1',@statistics.api_calls['POST/call_b'].get_top_dyno_name, 'Top dyno for POST/call_b is wrong')
  end
  def test_2
    @statistics = PlabHerokuStatistics.new
    call = ApiCall.new

    call.name = 'GET/call_a'
    call.dyno = 'web.11'
    call.time = 1
    @statistics.function_call call

    call.time += 1
    @statistics.function_call call

    call.time += 1
    @statistics.function_call call

    assert_equal(2, @statistics.api_calls['GET/call_a'].get_average_time, 'Average time calculated incorectly')
    assert_equal(2, @statistics.api_calls['GET/call_a'].get_median_time, 'Median time calculated incorectly')
  end
end
